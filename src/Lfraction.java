import java.util.*;

/** This class represents fractions of form n/d where n and d are long integer 
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
      Lfraction a = new Lfraction(4L, 10L);
      valueOf("123/1243/");
      a.shorten(a);
      System.out.println(a.den);
      // TODO!!! Your debugging tests here
   }

   // TODO!!! instance variables here

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   private long num;
   private long den;

   public Lfraction (long a, long b) {
      // TODO!!!
      if (b == 0)
         throw new RuntimeException("Denominator is 0.");
      if (b < 0){
         a *= -1;
         b *= -1;
      }
      long shorten = shorter(a,b);
      this.num = a / shorten;
      this.den = b / shorten;
   }

   public static long shorter(long a, long b) {
      return b == 0 ? a : Math.abs(shorter(b, a % b));
   }

   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {
      return this.num; // TODO!!!
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public long getDenominator() { 
      return this.den; // TODO!!!
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      String a = Long.toString(this.num) + "/" + Long.toString(this.den);
      return a; // TODO!!!
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
      Lfraction second = (Lfraction) m;
      second.shorten(second);
      Lfraction first = new Lfraction(this.num, this.den);
      first.shorten(first);
      return (second.compareTo(first) == 0);
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return Objects.hash(this.num, this.den); // TODO!!!
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
      Lfraction a = new Lfraction(this.num, this.den);
      if (this.den == m.den)
      {
         a.num = a.num + m.num;
         return a.shorten(a);
      }
      else
         {
         a.num = (a.num * m.den) + (m.num * a.den);
         a.den = m.den * a.den;
         return a.shorten(a);
         }
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      Lfraction a = new Lfraction(this.num * m.num, this.den * m.den);
      return a.shorten(a); // TODO!!!
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      if (this.num == 0L)
         throw new RuntimeException("Denominator can not be 0");
      Lfraction a = new Lfraction(this.den, this.num);
      return a.shorten(a); // TODO!!!
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(this.num * -1, this.den); // TODO!!!
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
      m = m.opposite();
      Lfraction a = new Lfraction(this.num, this.den);
      a = a.plus(m);
      return a.shorten(a);
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
      if (m.num == 0)
         throw new RuntimeException("Division by 0");
      Lfraction a = new Lfraction(this.num, this.den);
      return a.times(m.inverse()); // TODO!!!
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
      Lfraction a = new Lfraction(this.num, this.den);
      if (a.shorten(a).num == m.shorten(m).num && a.den == m.den)
         return 0;
      Lfraction dif = a.minus(m);
      if (dif.num > 0)
         return 1;
      else
         return -1;
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(this.num, this.den); // TODO!!!
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {
      return this.num/this.den; // TODO!!!
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      Lfraction a = new Lfraction(this.num % this.den, this.den);
      return a.shorten(a); // TODO!!!
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      return (double)this.num/(double)this.den; // TODO!!!
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      return new Lfraction(Math.round(f * d), d); // TODO!!!
   }

   /** Convpublic void testLfractionPart() {
      Lfraction f1 = new Lfraction (2, 3);
      Lfraction i = f1.fractionPart();
      assertEquals ("wrong fraction part ", new Lfraction (2, 3), i);
      f1 = new Lfraction (3, 2);
      i = f1.fractionPart();
      assertEquals ("wrong fraction part ", new Lfraction (1, 2), i);
      f1 = new Lfraction (32, 3);
      i = f1.fractionPart();
      assertEquals ("wrong fraction part ", new Lfraction (2, 3), i);
      f1 = new Lfraction (33, 3);
      i = f1.fractionPart();
      assertEquals ("wrong fraction part ", new Lfraction (0, 1), i);
      f1 = new Lfraction (-33, 3);
      i = f1.fractionPart();
      assertEquals ("wrong fraction part ", new Lfraction (0, 1), i);
      f1 = new Lfraction (-5, 4);
      i = f1.fractionPart();
      assertTrue ("wrong fraction part " + i.toString()
         + " for " + f1.toString(),
         i.equals (new Lfraction (-1, 4)) || i.equals (new Lfraction (3, 4)));
   }ersion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      if (!s.contains("/"))
         throw new RuntimeException("Unable to convert " + s);
      String[] nums = s.split("/");
      if (nums.length > 2)
         throw new RuntimeException("Too many symbols in " + s);
      for (String str: nums)
      {
         try {
            Long.parseLong(str);
         }
         catch (NumberFormatException e){
            throw new RuntimeException("Illegal symbol in " + str + " in " + s);
         }
      }
      return new Lfraction(Long.parseLong(nums[0]), Long.parseLong(nums[1])); // TODO!!!
   }

   public Lfraction shorten(Lfraction a){
      if (a.num == 0)
      {
         a.den = 1L;
         return a;
      }
      long end;
      if (a.num > a.den) {
         end = a.den;
      }
      else {
         end = a.num;
      }
      if (end < 0)
         end *= -1;
      for (long i = 1; i < end + 1; i++) {
         if (a.num % i == 0 && a.den % i == 0)
         {
            a.num /= i;
            a.den /= i;
         }
      }
      return a;
   }
}

